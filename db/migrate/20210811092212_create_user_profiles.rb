class CreateUserProfiles < ActiveRecord::Migration[6.1]
  def change
    create_table :user_profiles do |t|
      t.string :bio
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
