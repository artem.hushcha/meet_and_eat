class CreateGroupsMeetings < ActiveRecord::Migration[6.1]
  def change
    create_table :groups_meetings do |t|
      t.integer :meeting_id
      t.integer :group_id

      t.timestamps
    end
  end
end
