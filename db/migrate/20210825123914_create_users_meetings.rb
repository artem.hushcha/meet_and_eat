class CreateUsersMeetings < ActiveRecord::Migration[6.1]
  def change
    create_table :users_meetings do |t|
      t.integer :meeting_id
      t.integer :user_id

      t.timestamps
    end
  end
end
