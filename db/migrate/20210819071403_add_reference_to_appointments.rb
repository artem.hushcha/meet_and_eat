class AddReferenceToAppointments < ActiveRecord::Migration[6.1]
  def change
    change_table :appointments do |t|
      t.integer :location_id
      t.integer :meeting_id
    end
  end
end