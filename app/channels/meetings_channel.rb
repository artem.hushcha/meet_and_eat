# MeetingsChannel
class MeetingsChannel < ApplicationCable::Channel
  def subscribed
    current_user.meetings.each do |meeting|
      stream_from "meetings:#{meeting.id}"
    end
  end

  def unsubscribed
    stop_all_streams
  end
end
