# GroupsController
class GroupsController < ApplicationController
  before_action :current_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @groups = Group.all
  end

  def create
    @group = current_user.owned_groups.new(group_params)

    if @group.save
      @group.users = group_members
      redirect_to groups_path, flash: { notice: 'Group was successfully created.' }
    else
      redirect_to groups_path, flash: { error: @group.errors.full_messages.to_sentence }
    end
  end

  def update
    if @group.update(group_params)
      @group.users = group_members
      redirect_to groups_path, flash: { notice: 'Group was successfully updated.' }
    else
      redirect_to edit_group_path, flash: { error: @group.errors.full_messages.to_sentence }
    end
  end

  def destroy
    @group.delete
    redirect_to groups_path, flash: { notice: 'Group was successfully deleted.' }
  end

  private

  def group_params
    params.require(:group).permit(:title, :description)
  end

  def group_members
    User.where(id: params[:group][:users].reject(&:empty?))
  end

  def current_group
    @group = Group.find(params[:id])
  end
end
