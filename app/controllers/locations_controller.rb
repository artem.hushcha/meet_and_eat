# Location controller
class LocationsController < ApplicationController
  def index
    @locations = Location.all
  end

  def show
    @location = Location.find(params[:id])
  end

  def new
    @location = Location.new
  end

  def create
    @location = Location.new({ title: location_params[:title], address: location_params[:address], latitude: location_params[:latitude], longitude: location_params[:longitude] })
    if @location.save
      redirect_to locations_path, flash: { notice: "Successfully created location." }
    else
      render action: 'new', flash: { error: @location.errors.full_messages.to_sentence }
    end
  end

  def edit
    @location = Location.find(params[:id])
  end

  def update
    @location = Location.find(params[:id])
    if @location.update(location_params)
      redirect_to @location, flash: { notice: "Successfully updated location." }
    else
      render action: 'edit'
    end
  end

  def destroy
    @location = Location.find(params[:id])
    @location.delete
    redirect_to locations_url, flash: { notice: "Successfully destroyed location." }
  end

  private

  def location_params
    params.require(:location).permit(:title, :address, :latitude, :longitude)
  end
end
