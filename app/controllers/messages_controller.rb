# MessagesController
class MessagesController < ApplicationController
  before_action :set_meeting
  before_action :authenticate_user!

  def create
    message = @meeting.messages.new(message_params)
    message.user = current_user
    message.save
    MessageRelayJob.perform_later(message)
  end

  private

  def set_meeting
    @meeting = Meeting.find(params[:meeting_id])
  end

  def message_params
    params.require(:message).permit(:body)
  end
end
