# Meetings controller
class MeetingsController < ApplicationController
  before_action :current_meeting, only: [:show, :edit, :update, :destroy]

  def index
    @meetings = Meeting.all
  end

  def create
    @meeting = Meeting.new({ title: meeting_params[:title], description: meeting_params[:description] })

    if @meeting.save
      @meeting.users = meeting_members
      send_invitations
      redirect_to meetings_path, flash: { notice: 'Meeting was successfully created.' }
    else
      redirect_to meetings_path, flash: { error: @meeting.errors.full_messages.to_sentence }
    end
  end

  def update
    if @meeting.update({ title: meeting_params[:title], description: meeting_params[:description] })
      resend_invitations
      @meeting.users = meeting_members
      redirect_to meetings_path, flash: { notice: 'Meeting was successfully updated.' }
    else
      redirect_to edit_meeting_path, flash: { error: @meeting.errors.full_messages.to_sentence }
    end
  end

  def destroy
    @meeting.delete
    redirect_to meetings_path, flash: { notice: 'Meeting was successfully deleted.' }
  end

  private

  def send_invitations
    meeting_members.each do |user|
      UserMailer.with(user: user, meeting: @meeting).meeting_email.deliver_now
    end
  end

  def resend_invitations
    meeting_members.each do |user|
      UserMailer.with(user: user, meeting: @meeting).updated_meeting_email.deliver_now
    end
  end

  def meeting_params
    params.require(:meeting).permit(:title, :description, users: [])
  end

  def meeting_members
    User.where(id: meeting_params[:users].reject(&:empty?))
  end

  def current_meeting
    @meeting = Meeting.find(params[:id])
  end
end
