# List for selecting meeting members
module MeetingsHelper
  def users_select_list
    User.all.collect { |u| [u.email, u.id] }
  end

  def groups_select_list
    Group.all.collect { |g| [g.title, g.id] }
  end
end
