# List for selecting group members
module GroupsHelper
  def users_select_list
    User.all.collect { |u| [u.email, u.id] }
  end
end
