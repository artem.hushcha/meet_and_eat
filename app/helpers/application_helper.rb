module ApplicationHelper
  def bootstrap_class_for_flash(flash_type)
    cases = {
      'success' => 'alert-success',
      'error' => 'alert-danger',
      'alert' => 'alert-warning',
      'notice' => 'alert-info'
    }
    cases[flash_type]
  end
end
