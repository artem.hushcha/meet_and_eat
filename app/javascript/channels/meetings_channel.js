import consumer from "./consumer"

consumer.subscriptions.create("MeetingsChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.log("Connected to the meeting!");
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    $("[data-behavior='messages'][data-meeting-id='#{data.meeting_id}']").append(data.message)
  }
});
