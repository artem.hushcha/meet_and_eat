# Mailer
class UserMailer < ApplicationMailer
  default from: 'from@example.com'

  def meeting_email
    send_email_with('Please take a look, you were added to meeting')
  end

  def updated_meeting_email
    send_email_with('Please take a look, you were added to changed meeting')
  end

  private

  def send_email_with(msg)
    @user = params[:user]
    @meeting = params[:meeting]
    mail(to: @user.email, subject: msg)
  end
end
