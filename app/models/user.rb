# User
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  validates_presence_of :name, :email, :phone

  has_one  :user_profile
  has_many :memberships
  has_many :groups, through: :memberships
  has_many :owned_groups, foreign_key: :owner_id, class_name: 'Group'
  has_many :users_meetings
  has_many :meetings, through: :users_meetings
  has_one_attached :avatar
  has_many :messages

  def avatar_thumbnail
    if avatar.attached?
      avatar.variant(resize: "150x150!").processed
    else
      'default_profile.jpg'
    end
  end
end
