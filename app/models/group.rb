class Group < ApplicationRecord
  validates_presence_of :title
  has_many :memberships
  has_many :users, through: :memberships
  has_many :groups_meetings
  has_many :meetings, through: :groups_meetings

  belongs_to :user, foreign_key: :owner_id
end
