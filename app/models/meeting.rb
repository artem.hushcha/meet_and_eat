class Meeting < ApplicationRecord
  validates_presence_of :title
  has_many :appointments
  has_many :locations, through: :appointments
  has_many :users_meetings
  has_many :users, through: :users_meetings
  has_many :groups_meetings
  has_many :groups, through: :groups_meetings
  has_many :messages
end
