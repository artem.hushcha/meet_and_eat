class UserProfile < ApplicationRecord
  belongs_to :user
  validates_presence_of :bio
end
