# Location
class Location < ApplicationRecord
  validates_presence_of :title
  has_many :appointments
  has_many :meetings, through: :appointments
  geocoded_by :address
  reverse_geocoded_by :latitude, :longitude
  after_validation :geocode, :reverse_geocode
end
