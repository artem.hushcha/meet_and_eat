# MessageRelayJob
class MessageRelayJob < ApplicationJob
  queue_as :default

  def perform(message)
    ActionCable.server.broadcast "meetings:#{message.meeting.id}", {
      message: MessagesController.render(message),
      meeting_id: message.meeting.id
    }
  end
end
