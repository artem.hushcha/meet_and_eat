# README
MEET AND EAT 
    This project are created by student's of Pivorak Summer Course 2021.

* Ruby version - 2.7.3

    Steps describe:
     1   -  Cloning project into local environment
    2-4  -  Installing system dependencies for the project
    5-9  -  Database (PostgreSQL) configuration
     10  -  Database creation
     11  -  Running web application 


    SETUP PROJECT:

1. Cloning project into your's place:

    git clone git@gitlab.com:artem.hushcha/meet_and_eat.git

2. Installing PostgreSQL database:

    brew install postgresql

3. Installing required project packages:

    bundle install

4. Update required project packages:

    bundle update

5. Export PATH into your's bash profile:

    echo 'export PATH="/usr/local/opt/postgresql@13.4/bin:$PATH"' >> ~/.bash_profile

6. Apply changes into your's bash profile:

    source ~/.bash_profile

7. Start PostgreSQL service:

    brew services start postgresql@13

8. Create user with role eat_and_meat:

    createuser -P -d meet_and_eat

9. Create/Configure database file:

    vi config/database.yml

    Add follow lines:

      default: &default
        adapter: postgresql
        encoding: unicode
        username: meet_and_eat
        password: password

      development:
        <<: *default
        database: meet_and_eat_development

      test:
        <<: *default
        database: meet_and_eat_test

10. Create database:

    rake db:setup
    rake db:migrate

11. Start your's web-server:

    rails server --binding=127.0.0.1


    TESTING PROJECT

*  To run tests locally:

    bundle exec rspec

*  To run rubocop locally:

    bundle exec rubocop
