require 'rails_helper'

describe 'Registration flow: ' do
  it 'User is signing up with correct data' do
    visit('/users/sign_up')

    within('#new_user') do
      fill_in 'Name', with: 'John'
      fill_in 'Phone', with: '0634353535'
      fill_in 'Email', with: 'john@example.com'
      fill_in 'Password', with: 'sEcReT321'
      fill_in 'Password confirmation', with: 'sEcReT321'
    end

    click_button 'Sign up'

    expect(page).to(have_content('A message with a confirmation link has been sent to your email address.'))
  end

  it 'User is signing up without filling required information' do
    visit('/users/sign_up')

    click_button 'Sign up'

    expect(page).to(have_content("Email can't be blank"))
    expect(page).to(have_content("Password can't be blank"))
    expect(page).to(have_content("Name can't be blank"))
    expect(page).to(have_content("Phone can't be blank"))
  end
end
