require 'rails_helper'

describe 'Group: ' do
  before do
    @user_john = User.create(email: 'john1@example.com', password: 'sEcReT321', name: 'John', phone: '0634353535')
    @user_john.confirm
    @user_jassy = User.create(email: 'jassy@example.com', password: 'SeCrEt123', name: 'Jassy', phone: '0934353535')
    @user_jassy.confirm
    @user_jim = User.create(email: 'jim@example.com', password: 'SecrcT213', name: 'jim', phone: '0734353535')
    @user_jim.confirm

    visit '/users/sign_in'

    within('#new_user') do
      fill_in 'Email',    with: @user_john.email
      fill_in 'Password', with: @user_john.password
    end

    click_button 'Log in'
  end

  it 'Does not create an invalid group' do
    visit '/groups/new'

    click_button 'create'

    expect(page).to(have_content("Title can't be blank"))
  end

  it 'Creates a new group with members' do
    visit '/groups/new'

    within('#group_section') do
      fill_in 'Title', with: 'Some title'
      fill_in 'Description', with: 'Some description'
      page.select 'jassy@example.com', from: 'Users'
      page.select 'john1@example.com', from: 'Users'
    end

    click_button 'create'

    expect(page).to(have_content('Group was successfully created.'))
  end

  let!(:group) { @user_john.owned_groups.create(title: 'title', description: 'description') }

  xit 'Updates the group' do
    visit edit_group_path(group.id)

    within('#group_section') do
      fill_in 'Title', with: 'Another title'
      fill_in 'Description', with: 'Another description'
      page.select 'jim@example.com', from: 'Users'
      page.select 'john1@example.com', from: 'Users'
    end

    click_button 'edit'

    expect(page).to(have_content('Group was successfully updated.'))
  end
end
