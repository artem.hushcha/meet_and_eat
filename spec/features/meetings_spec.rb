require 'rails_helper'

describe 'Meeting:', type: :feature do
  before do
    @user_john = User.create(email: 'john1@example.com', password: 'sEcReT321', name: 'John', phone: '0678912345')
    @user_john.confirm
    @user_jassy = User.create(email: 'jassy@example.com', password: 'SeCrEt123', name: 'Jassy', phone: '0998765432')
    @user_jassy.confirm
    @user_jim = User.create(email: 'jim@example.com', password: 'SecreT213', name: 'Jim', phone: '0965432109')
    @user_jim.confirm
  end

  it 'Does not create an invalid meeting' do
    visit '/meetings/new'

    click_button 'create'

    expect(page).to(have_content("Title can't be blank"))
  end

  it 'Creates a new meeting with members' do
    visit '/meetings/new'

    within('#meeting_section') do
      fill_in 'Title', with: 'Some title'
      fill_in 'Description', with: 'Some description'
      page.select 'jassy@example.com', from: 'Users'
      page.select 'john1@example.com', from: 'Users'
    end

    click_button 'create'

    expect(page).to(have_content('Meeting was successfully created.'))
  end

  let!(:meeting) { Meeting.create(title: 'title', description: 'description') }

  xit 'Updates the meeting' do
    visit edit_meeting_path(meeting.id)

    within('#meeting_section') do
      fill_in 'Title', with: 'Another title'
      fill_in 'Description', with: 'Another description'
      page.select 'jim@example.com', from: 'Users'
      page.select 'john1@example.com', from: 'Users'
    end

    click_button 'edit'

    expect(page).to(have_content('Meeting was successfully updated.'))
  end
end
