require 'rails_helper'

describe 'Authorization flow: ' do
  before do
    @user = User.create(email: 'john1@example.com', password: 'sEcReT321', name: 'John', phone: '0634353535')
    @user.confirm
  end

  let(:other_user) { User.new(email: 'other@example.com', password: 'secret', name: 'Jassy', phone: '0934353535') }

  it 'Signing in with correct credentials' do
    visit '/users/sign_in'

    within('#new_user') do
      fill_in 'Email',    with: @user.email
      fill_in 'Password', with: @user.password
    end

    click_button 'Log in'

    expect(page).to(have_content('Signed in successfully.'))
  end

  it 'Signing in as another user' do
    visit '/users/sign_in'

    within('#new_user') do
      fill_in 'Email', with: other_user.email
      fill_in 'Password', with: other_user.password
    end

    click_button 'Log in'

    expect(page).to(have_content('Forgot your password?'))
  end
end
