require 'rails_helper'

RSpec.describe Appointment, type: :model do
  context 'relations' do
    it { should belong_to(:meeting) }
    it { should belong_to(:location) }
  end
end
