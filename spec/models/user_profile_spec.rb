require 'rails_helper'

RSpec.describe UserProfile, type: :model do
  context 'relations' do
    it { is_expected.to(belong_to(:user)) }
  end

  context 'validations' do
    it { is_expected.to(validate_presence_of(:bio)) }
  end
end
