require 'rails_helper'

RSpec.describe User, type: :model do
  context 'relations' do
    it { is_expected.to(have_one(:user_profile)) }
    it { should have_many(:memberships) }
    it { should have_many(:groups).through(:memberships) }
    it { should have_many(:groups) }
    it { should have_many(:owned_groups) }
  end

  context 'validations' do
    it { is_expected.to(validate_presence_of(:name)) }
    it { is_expected.to(validate_presence_of(:email)) }
    it { is_expected.to(validate_presence_of(:phone)) }
  end
end
