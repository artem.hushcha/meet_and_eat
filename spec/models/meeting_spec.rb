require 'rails_helper'

RSpec.describe Meeting, type: :model do
  context 'relations' do
    it { should have_many(:appointments) }
    it { should have_many(:locations).through(:appointments) }
  end
  context 'validations' do
    it { is_expected.to(validate_presence_of(:title)) }
  end
end
