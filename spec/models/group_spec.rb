require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'relations' do
    it { should have_many(:memberships) }
    it { should have_many(:users).through(:memberships) }
    it { should belong_to(:user) }
  end

  context 'validations' do
    it { is_expected.to(validate_presence_of(:title)) }
  end
end
