require 'rails_helper'

RSpec.describe Location, type: :model do
  context 'relations' do
    it { should have_many(:appointments) }
    it { should have_many(:meetings).through(:appointments) }
  end
  xcontext 'validations' do
    it { is_expected.to(validate_presence_of(:title)) }
  end
end
