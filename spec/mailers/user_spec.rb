require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  before do
    @user = User.create(email: 'to@example.com', password: 'sEcReT321', name: 'John', phone: '0634353535')
    @user.confirm
    @meeting = Meeting.create(title: 'title', description: 'description')
  end

  describe "meeting_email" do
    let(:mail) { UserMailer.with(user: @user, meeting: @meeting).meeting_email }

    it "renders the headers" do
      expect(mail.subject).to eq("Please take a look, you were added to meeting")
      expect(mail.to).to eq(["to@example.com"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Welcome")
    end
  end

  describe "updated_meeting_email" do
    let(:mail) { UserMailer.with(user: @user, meeting: @meeting).updated_meeting_email }

    it "renders the headers" do
      expect(mail.subject).to eq("Please take a look, you were added to changed meeting")
      expect(mail.to).to eq(["to@example.com"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Welcome")
    end
  end
end
