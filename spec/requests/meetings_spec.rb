require 'rails_helper'

RSpec.describe 'Meeting', type: :request do
  before do
    @user_john = User.create(email: 'john1@example.com', password: 'sEcReT321', name: 'John', phone: '0678912345')
    @user_john.confirm
    @user_jassy = User.create(email: 'jassy@example.com', password: 'SeCrEt123', name: 'Jassy', phone: '0998765432')
    @user_jassy.confirm
    @user_jim = User.create(email: 'jim@example.com', password: 'SecreT213', name: 'Jim', phone: '0965432109')
    @user_jim.confirm
  end

  let(:meeting_params) do
    { meeting: { title: 'Title', description: 'Description', users: ['', @user_john.id.to_s, @user_jassy.id.to_s] } }
  end

  let(:invalid_meeting_params) do
    { meeting: { title: '', description: 'Another description', users: ['', @user_john.id.to_s, @user_jim.id.to_s] } }
  end

  # create action
  describe '#create' do
    context 'when params are correct' do
      subject do
        post "/meetings", params: meeting_params
      end

      it 'creates new meeting successfully' do
        expect { subject }.to change { Meeting.count }.by(1)
        expect(response).to redirect_to(meetings_path)
      end

      it 'sets correct data' do
        subject
        meeting = Meeting.last
        expect(meeting.title).to eq('Title')
        expect(meeting.description).to eq('Description')
        expect(meeting.users).to(include(@user_jassy, @user_john))
        expect(meeting.users).to_not(include(@user_jim))
      end

      it 'sends email to each member' do
        expect { subject }.to change { ActionMailer::Base.deliveries.count }.by(2)
        expect(ActionMailer::Base.deliveries.last.subject).to eq("Please take a look, you were added to meeting")
        expect(ActionMailer::Base.deliveries.last(2).collect(&:to)).to match_array([[@user_john.email], [@user_jassy.email]])
      end
    end

    context 'when params are not correct' do
      subject do
        post "/meetings", params: invalid_meeting_params
      end

      it 'does not create new user' do
        expect { subject }.to change { Meeting.count }.by(0)
      end
    end
  end

  # update action
  describe '#update' do
    let(:updated_meeting_params) do
      { meeting: { title: 'Another title', description: 'Another description', users: ['', @user_john.id.to_s, @user_jim.id.to_s] } }
    end

    before do
      post "/meetings", params: meeting_params
      @meeting = Meeting.last
    end

    context 'when updeted data is valid' do
      it 'updates meeting' do
        put "/meetings/#{@meeting.id}", params: updated_meeting_params
        @meeting.reload

        expect(@meeting.title).to eq('Another title')
        expect(@meeting.description).to eq('Another description')
        expect(@meeting.users).to(include(@user_jim, @user_john))
        expect(@meeting.users).to_not(include(@user_jassy))
      end

      it 'sends email to each member' do
        expect do
          put "/meetings/#{@meeting.id}", params: updated_meeting_params
        end.to change { ActionMailer::Base.deliveries.count }.by(2)
        expect(ActionMailer::Base.deliveries.last.subject).to eq("Please take a look, you were added to changed meeting")
        expect(ActionMailer::Base.deliveries.last(2).collect(&:to)).to match_array([[@user_john.email], [@user_jim.email]])
      end
    end

    context 'when data is invalid' do
      it 'does not update meeting' do
        put "/meetings/#{@meeting.id}", params: invalid_meeting_params
        @meeting.reload

        expect(@meeting.title).to eq('Title')
      end
    end
  end
end
