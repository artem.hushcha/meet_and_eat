require 'rails_helper'

RSpec.describe 'Group management', type: :request do
  before do
    @user_john = User.create(email: 'john1@example.com', password: 'sEcReT321', name: 'John', phone: '0634353535')
    @user_john.confirm
    sign_in @user_john
    @user_jassy = User.create(email: 'jassy@example.com', password: 'SeCrEt123', name: 'Jassy', phone: '0934353535')
    @user_jassy.confirm
    @user_jim = User.create(email: 'jim@example.com', password: 'SecrcT213', name: 'jim', phone: '0734353535')
    @user_jim.confirm
  end

  let(:group_params) do
    { group: { title: 'Title', description: 'Description', users: ['', @user_john.id.to_s, @user_jassy.id.to_s] } }
  end

  let(:invalid_group_params) do
    { group: { title: '', description: 'Another description', users: ['', @user_john.id.to_s, @user_jim.id.to_s] } }
  end

  # create action
  describe '#create' do
    context 'when params are correct' do
      subject do
        post "/groups", params: group_params
      end

      it 'creates new group successfully' do
        expect { subject }.to change { Group.count }.by(1)
        expect(response).to redirect_to(groups_path)
      end

      it 'sets correct data' do
        subject
        group = Group.last
        expect(group.title).to eq('Title')
        expect(group.description).to eq('Description')
        expect(group.users).to(include(@user_jassy, @user_john))
        expect(group.users).to_not(include(@user_jim))
      end
    end

    context 'when params are not correct' do
      subject do
        post "/groups", params: invalid_group_params
      end

      it 'does notcreate new user' do
        expect { subject }.to change { Group.count }.by(0)
      end
    end
  end

  # update action
  describe '#update' do
    let(:updated_group_params) do
      { group: { title: 'Another title', description: 'Another description', users: ['', @user_john.id.to_s, @user_jim.id.to_s] } }
    end

    before do
      post "/groups", params: group_params
      @group = Group.last
    end

    context 'when updeted data is valid' do
      it 'updates group' do
        put "/groups/#{@group.id}", params: updated_group_params
        @group.reload

        expect(@group.title).to eq('Another title')
        expect(@group.description).to eq('Another description')
        expect(@group.users).to(include(@user_jim, @user_john))
        expect(@group.users).to_not(include(@user_jassy))
      end
    end

    context 'when data is invalid' do
      it 'does not update group' do
        put "/groups/#{@group.id}", params: invalid_group_params
        @group.reload

        expect(@group.title).to eq('Title')
      end
    end
  end
end
